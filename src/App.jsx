import Header from './components/header';
import './App.css';

import Accueil from './pages/Accueil';

import { useState } from 'react';
import ProjetWeb from './pages/Projet.Web';
import ProjetDestop from './pages/projetDestop';

export default function App() {

  const [pageCourant ,setPageCourant]=useState("Accueil")

  const ChangePage =(page)=>{
    return ()=>{
      setPageCourant(page)

    }
    
  }

 

  return (
    <div className="App">
      <Header ChangePage  = {ChangePage }/>

      <ul>
        <li>
          <a href='#/' onClick={ChangePage("Accueil")}>Accueil</a>
        </li>
        <li>
          <a href='#/' onClick={ChangePage("ProjetWeb")}>projet web</a>
        </li>

        <li>
          <a href='#/' onClick={ChangePage("ProjetDestop")}>projet Destop</a>
        </li>
      </ul>

      {pageCourant==="Accueil" && <Accueil nom="hassan" />}
      {pageCourant==="ProjetWeb" && <ProjetWeb nom="hassan" />}
      {pageCourant==="ProjetDestop" && <ProjetDestop nom="hassan" />}


      


      

    </div>

  );
}


