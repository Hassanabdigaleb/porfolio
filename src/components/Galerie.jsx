import styles from "./Galerie.module.css"

export default function Galerie(props){
    

    return <div className={styles.galerie} >
       
    {props.image.map((element) =>{

        return <img src={element.src} alt={element.alt}/>

    
    })}
   </div>   
}